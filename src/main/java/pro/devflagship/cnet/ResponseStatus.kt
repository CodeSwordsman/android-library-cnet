package pro.devflagship.cnet

enum class ResponseStatus
{
    OK,
    INTERNAL_ERROR,
    EXTERNAL_ERROR,
    NULL,
    NO_INTERNET
}