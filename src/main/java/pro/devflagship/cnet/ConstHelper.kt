package pro.devflagship.cnet

internal class ConstHelper
{
    companion object
    {
        const val HTTP_PROTOCOL = "http://"
        const val HTTPS_PROTOCOL = "https://"
        const val GOOGLE = "https://google.com"
        const val EMPTY = ""
        const val CONTENT_TYPE = "Content-Type"
        const val CHARSET_EQUAL = "charset="
        const val APPLICATION_JSON_TYPE = "application/json;"
        const val CHARSET_UTF8 = "UTF-8"

        const val HTTP_REQUEST_TYPE_GET="GET"
        const val HTTP_REQUEST_TYPE_POST="POST"
        const val HTTP_REQUEST_TYPE_DELETE="DELETE"
        const val HTTP_REQUEST_TYPE_PUT="PUT"
        const val HTTP_REQUEST_TYPE_HEAD="HEAD"
        const val HTTP_REQUEST_TYPE_CONNECT="CONNECT"
        const val HTTP_REQUEST_TYPE_OPTIONS="OPTIONS"
        const val HTTP_REQUEST_TYPE_TRACE="TRACE"

        const val SLESH_CHAR = '/'
        const val BACKSLESH_CHAR = '\\'
        const val QUESTION_MARK_CHAR = '?'
        const val APPERSAND_CHAR = '&'
        const val EQUALS_SIGN_CHAR = '='


        const val HTTP_RESPONSE_CODE_ERROR = -1
        const val HTTP_RESPONSE_CODE_CONTINUE = 100
        const val HTTP_RESPONSE_CODE_SWITCHING_PROTOCOL = 101
        const val HTTP_RESPONSE_CODE_OK = 200
        const val HTTP_RESPONSE_CODE_CREATED = 201
        const val HTTP_RESPONSE_CODE_ACCEPTED = 202
        const val HTTP_RESPONSE_CODE_NON_AUTHORIATIVE_INFORMATION = 203
        const val HTTP_RESPONSE_CODE_NO_CONTENT = 204
        const val HTTP_RESPONSE_CODE_RESET_CONTENT = 205
        const val HTTP_RESPONSE_CODE_PARTIAL_CONTENT = 206
        const val HTTP_RESPONSE_CODE_MULTIPLE_CHOICES = 300
        const val HTTP_RESPONSE_CODE_MOVED_PERMANENTLY = 301
        const val HTTP_RESPONSE_CODE_FOUND = 302
        const val HTTP_RESPONSE_CODE_SEE_OTHER = 303
        const val HTTP_RESPONSE_CODE_NOT_MODIFIED = 304
        const val HTTP_RESPONSE_CODE_USE_PROXY = 305
        const val HTTP_RESPONSE_CODE_UNUSED = 306
        const val HTTP_RESPONSE_CODE_TEMPORARY_REDIRECT = 307
        const val HTTP_RESPONSE_CODE_UNAUTHORIZED = 401
        const val HTTP_RESPONSE_CODE_PAYMENT_REQUIRED = 402
        const val HTTP_RESPONSE_CODE_FORBIDDEN = 403
        const val HTTP_RESPONSE_CODE_NOT_FOUND = 404
        const val HTTP_RESPONSE_CODE_METHOD_NOT_ALLOWED = 405
        const val HTTP_RESPONSE_CODE_NOT_ACCEPTABLE = 406
        const val HTTP_RESPONSE_CODE_PROXY_AUTHENTICATION_REQUIRED = 407
        const val HTTP_RESPONSE_CODE_REQUEST_TIMEOUT = 408
        const val HTTP_RESPONSE_CODE_CONFLICT = 409
        const val HTTP_RESPONSE_CODE_GONE = 410
        const val HTTP_RESPONSE_CODE_LENGTH_REQUIRED = 411
        const val HTTP_RESPONSE_CODE_PRECONDITION_FAILED = 412
        const val HTTP_RESPONSE_CODE_REQUEST_ENTITY_TOO_LARGE = 413
        const val HTTP_RESPONSE_CODE_REQUEST_URI_TOO_LONG = 414
        const val HTTP_RESPONSE_CODE_UNSUPPORTED_MEDIA_TYPE = 415
        const val HTTP_RESPONSE_CODE_REQUESTED_RANGE_NOT_SATISFIABLE = 416
        const val HTTP_RESPONSE_CODE_EXPECTATION_FAILED = 417
        const val HTTP_RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500
        const val HTTP_RESPONSE_CODE_NOT_IMPLEMENTED = 501
        const val HTTP_RESPONSE_CODE_BAD_GATEWAY = 502
        const val HTTP_RESPONSE_CODE_SERVICE_UNAVAILABLE = 503
        const val HTTP_RESPONSE_CODE_GATEWAY_TIMEOUT = 504
        const val HTTP_RESPONSE_CODE_HTTP_VERSION_NOT_SUPPORTED = 505
    }
}