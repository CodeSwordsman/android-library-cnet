package pro.devflagship.cnet


enum class NetMethodType(val method:String)
{
    GET(ConstHelper.HTTP_REQUEST_TYPE_GET),
    POST(ConstHelper.HTTP_REQUEST_TYPE_POST),
    DELETE(ConstHelper.HTTP_REQUEST_TYPE_DELETE),
    PUT(ConstHelper.HTTP_REQUEST_TYPE_PUT),
    HEAD(ConstHelper.HTTP_REQUEST_TYPE_HEAD),
    CONNECT(ConstHelper.HTTP_REQUEST_TYPE_CONNECT),
    OPTIONS(ConstHelper.HTTP_REQUEST_TYPE_OPTIONS),
    TRACE(ConstHelper.HTTP_REQUEST_TYPE_TRACE)
}