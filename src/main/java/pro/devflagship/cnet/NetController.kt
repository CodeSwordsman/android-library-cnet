package pro.devflagship.cnet

import android.os.AsyncTask
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import javax.net.ssl.HttpsURLConnection

class NetController
{
    companion object
    {
        private var testInternetPath : URL = URL(ConstHelper.GOOGLE)
        private var apiRootPath : String = ConstHelper.EMPTY
        var requestError :String = ConstHelper.EMPTY

        @JvmStatic
        fun setTestInternetPath(path:String)
        {
            testInternetPath = URL(path)
        }

        @JvmStatic
        fun setTestInternetPath(path:URL)
        {
            testInternetPath = path
        }

        @JvmStatic
        fun setApiRootPath(path:String)
        {
            apiRootPath = path
            apiRootPath = apiRootPath.trim()
            if(apiRootPath.last() != ConstHelper.SLESH_CHAR && apiRootPath.last() != ConstHelper.BACKSLESH_CHAR)
            {
                apiRootPath = apiRootPath.plus(ConstHelper.SLESH_CHAR)
            }
            if(!apiRootPath.startsWith(ConstHelper.HTTP_PROTOCOL,true) && !apiRootPath.startsWith(ConstHelper.HTTPS_PROTOCOL,true))
            {
                apiRootPath = "${ConstHelper.HTTP_PROTOCOL}$apiRootPath"
            }
        }

        @JvmStatic
        fun getApiRootPath():String
        {
            return apiRootPath
        }

        @JvmStatic
        fun getTestInternetPath():URL
        {
            return testInternetPath
        }

        @JvmStatic
        fun doJSONRequest(path: String, data:String, afterMethod: ((result: Response) -> Unit?)? = null)
        {
            doRequest<String>(
                path,
                false,
                NetMethodType.POST,
                null,
                null,
                true,
                data,
                afterMethod
            )
        }

        @JvmStatic
        fun <T> doRequest(path: String,parameters: HashMap<String, T>?,afterMethod: ((result: Response) -> Unit?)? = null)
        {
            doRequest(
                path,
                false,
                NetMethodType.POST,
                null,
                parameters,
                false,
                ConstHelper.EMPTY,
                afterMethod
            )
        }

        @JvmStatic
        fun <T> doRequest(path: String, isAwait: Boolean=false, requestMethodType: NetMethodType = NetMethodType.POST, parameters: HashMap<String, T>?, isJSON: Boolean=false, data: String, afterMethod: ((result: Response) -> Unit?)? = null)
        {
            doRequest(
                path,
                isAwait,
                requestMethodType,
                null,
                parameters,
                isJSON,
                data,
                afterMethod
            )
        }

        @JvmStatic
        fun <T> doRequest(path:String, isAwait:Boolean = false, requestMethodType: NetMethodType = NetMethodType.POST, headers: HashMap<String,String>?, parameters: HashMap<String,T>?, isJSON:Boolean = false, data:String = ConstHelper.EMPTY, afterMethod: ((result: Response) -> Unit?)? = null)
        {
                val request : StringBuffer = StringBuffer("$apiRootPath$path")
                if(requestMethodType == NetMethodType.GET)
                {
                    if(parameters != null)
                    {
                        request.append(ConstHelper.QUESTION_MARK_CHAR)
                        request.append(getQuery(parameters))
                    }
                }
                val requestPath = URL(request.toString())
                class RequestTask :  AsyncTask<Void, StringBuffer, Response>()
                {
                    override fun doInBackground(vararg params: Void?): Response
                    {
                        return try
                        {
                            backTaskResp(
                                requestPath,
                                requestMethodType,
                                headers,
                                parameters,
                                isJSON,
                                data
                            )
                        }
                        catch (ex: Exception)
                        {
                            ex.printStackTrace()
                            Response(ResponseStatus.INTERNAL_ERROR,null, HttpResponseCode.HTTP_RESPONSE_CODE_ERROR)
                        }
                    }

                    override fun onPostExecute(result: Response)
                    {
                        if(afterMethod != null)
                        {
                            afterMethod(result)
                        }
                    }
                }
            if(isAwait){RequestTask().execute().get()}else{RequestTask().execute()}
        }

        @JvmStatic
        private fun <T> backTaskResp(url : URL, requestMethodType:NetMethodType = NetMethodType.POST, headers: HashMap<String,String>?, params: HashMap<String,T>?, isJSON:Boolean = false, data:String = ConstHelper.EMPTY) : Response
        {
            if(isOnline())
            {
                val connection = if(url.toString().startsWith(ConstHelper.HTTPS_PROTOCOL)){url.openConnection() as HttpsURLConnection} else{url.openConnection() as HttpURLConnection
                }
                connection.requestMethod = requestMethodType.method
                if(isJSON)
                {
                    connection.setRequestProperty(ConstHelper.CONTENT_TYPE, "${ConstHelper.APPLICATION_JSON_TYPE} ${ConstHelper.CHARSET_EQUAL}${ConstHelper.CHARSET_UTF8}")
                }

                if(headers != null)
                {
                    for (header in headers) {
                        connection.setRequestProperty(header.key, header.value)
                    }
                }

                if(connection.requestMethod != NetMethodType.GET.method)
                {
                    val os = if (isJSON) {
                        BufferedOutputStream(connection.outputStream)
                    } else {
                        connection.outputStream
                    }

                    val writer = BufferedWriter(OutputStreamWriter(os, ConstHelper.CHARSET_UTF8))

                    if (isJSON) {
                        writer.write(data)
                    } else {
                        if (params != null) {
                            writer.write(getQuery(params))
                        }
                    }
                    writer.flush()
                    writer.close()
                    os.close()
                }
                val response = StringBuffer()
                if(connection.inputStream != null) {
                    val bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
                    var inputLine: String

                    inputLine = bufferedReader.readLine()

                    while (inputLine != ConstHelper.EMPTY) {
                        response.append(inputLine)
                        inputLine = bufferedReader.readLine() ?: break
                    }
                    bufferedReader.close()
                    if(connection.errorStream != null)
                    {
                        requestError = connection.errorStream.toString()
                    }
                    connection.disconnect()
                }
                else
                {
                    return Response(ResponseStatus.NULL,null, HttpResponseCode.HTTP_RESPONSE_CODE_ERROR)
                }
                return Response(ResponseStatus.OK,response.toString(),HttpResponseCode.HTTP_RESPONSE_CODE_OK)
            }
            else
            {
                return Response(ResponseStatus.NO_INTERNET,null,HttpResponseCode.HTTP_RESPONSE_CODE_ERROR)
            }
        }

        @JvmStatic
        fun isOnlineAsync(requestMethodType: NetMethodType = NetMethodType.POST, headers:HashMap<String,String> = hashMapOf()): Boolean
        {
            class OnlineAsyncClass : AsyncTask<Void, StringBuffer, Boolean>()
            {
                override fun doInBackground(vararg voids: Void): Boolean
                {
                    try
                    {
                        val connection = testInternetPath.openConnection() as HttpsURLConnection
                        for (header in headers)
                        {
                            connection.setRequestProperty(header.key,header.value)
                        }
                        connection.requestMethod = requestMethodType.method
                        connection.connect()
                        connection.disconnect()
                        return true
                    }
                    catch (ex: Exception)
                    {
                        ex.printStackTrace()
                    }
                    return false
                }
            }
            return OnlineAsyncClass().execute().get()
        }

        @JvmStatic
        private fun isOnline(requestMethodType: NetMethodType = NetMethodType.POST, headers:HashMap<String,String> = hashMapOf()): Boolean
        {
            try
            {
                val connection = testInternetPath.openConnection() as HttpsURLConnection
                for (header in headers)
                {
                    connection.setRequestProperty(header.key,header.value)
                }
                connection.requestMethod = requestMethodType.method
                connection.connect()
                connection.disconnect()
                return true
            }
            catch (ex:Exception)
            {
                ex.printStackTrace()
            }
            return false
        }

        @JvmStatic
        private fun <T> getQuery(params: HashMap<String,T>): String {
            val result = StringBuilder()
            var first = true

            for (pair in params) {
                if (first)
                    first = false
                else
                    result.append(ConstHelper.APPERSAND_CHAR)

                result.append(URLEncoder.encode(pair.key, ConstHelper.CHARSET_UTF8))
                result.append(ConstHelper.EQUALS_SIGN_CHAR)
                result.append(URLEncoder.encode(pair.value.toString(), ConstHelper.CHARSET_UTF8))
            }
            return result.toString()
        }
    }
}